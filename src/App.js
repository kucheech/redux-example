import React from 'react';
import Counter from './Counter';
import Editor from './Editor';

function App() {
  return (
    <div style={{ textAlign: 'center' }}>
      <Counter />
      <hr />
      <Editor />
    </div>
  );
}

export default App;
