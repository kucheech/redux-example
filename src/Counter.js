import React from 'react';
import { connect } from 'react-redux';

const Counter = props => {
  return (
    <div>
      <h1>{props.counter}</h1>
    </div>
  );
};

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    counter: state.counter
  }
};

export default connect(mapStateToProps)(Counter);
