import React from 'react';
import { connect } from 'react-redux';
import { increment, decrement, reset } from './actionCreators';

const Editor = props => {
  return (
    <div>
      <button onClick={() => props.increment(1)}>Increment</button>
      <button onClick={() => props.decrement(1)}>Decrement</button>
      <button onClick={() => props.reset()}>Reset</button>
    </div>
  );
};

const mapDispatchToProps = { increment, decrement, reset };

export default connect(null, mapDispatchToProps)(Editor);
